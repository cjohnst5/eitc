*CREATE DLR_TRANSACTIONS

clear all
set obs 100
set seed 1234
set more off

gen dmvid = runiformint(111111111111111,999999999999999)
format dmvid %15.0f
label var dmvid "Unique DMV Record ID"

gen hhid = runiformint(111,999)
label var hhid "Unique HHI ID (based on address)"

gen pobox = . 
label var pobox "Registered Address is PO Box"

gen vehloc_yes = .
label var vehloc_yes "DMV data indicates actual vehicle location (for pobox)"

gen nontx = .
label var nontx "Registered Address is not in TX"

ralpha regpltno, upperonly l(7)
label var regpltno "Registered Plate Number"

gen regexpyr = . 
label var regexpyr "Registration Expiration Year"

gen regexpmo = . 
label var regexpmo "Registration Expiration Month"

ralpha vin, upperonly l(17)
label var vin "Vehicle Identification Number"

gen vinerr = . 
label var vinerr "Indicator of Check Digit Error"

gen ownrshpevidcd = "Texas Title"
label var ownrshpevidcd "Ownership Evidence Code"

gen bndedttlcd = ""
label var bndedttlcd "Bonded Title Code"

gen doctypecd = "Regular Title" if _n<50
replace doctypecd = "Registration Purposes Only" if _n>=50
label var doctypecd "Document Type Code"

gen ttlissuedateyr = runiformint(2006,2010)
label var ttlissuedateyr "title issue year(registration purposes only)"

gen ttlissuedatemo = runiformint(1,12)
label var ttlissuedatemo "title issue month(registration purposes only)"

gen ttlissuedatedom = runiformint(1,30)
label var ttlissuedatedom "title issue day of month (registration purposes only)"

gen ttlprocscd = ""
label var ttlprocscd "title process code"
 
gen prevownrname = "DEALER 1" if _n<=20
replace prevownrname = "DEALER 1" if _n>20 & _n<=70
replace prevownrname = "DEALER 2" if _n>70
label var prevownrname "previous owner name"

gen prevownrcity = "DENTON" if _n<=50
replace prevownrcity = "HOUSTON" if _n>50
label var prevownrcity "previous owner city"

gen prevownrstate = "TX" 
label var prevownrstate "previous owner state"

gen vehbdytype = "Coupe" if _n<=30
replace vehbdytype = "Truck" if _n>=30 & _n<60
replace vehbdytype = "SUV" if _n>=60
label var vehbdytype "vehicle body type"

gen vehclasscd = "PASS" if _n<=30
replace vehclasscd = "PASS TRK" if _n>=30 & _n<50
replace vehclasscd = "TRK<=1" if _n>=50 & _n<80
replace vehclasscd = "SEMI" if _n>=80 
label var vehclasscd "Vehicle Class Code"

gen vehmk = "HOND" if !mod(_n,2)
replace vehmk = "FORD" if mod(_n,2)
label var vehmk "vehicle make"

ralpha vehmodl, upperonly l(1)
label var vehmodl "Vehicle Model"

gen vehmodlyr = runiformint(2002,2006)
label var vehmodlyr "Model Year"

gen vehodmtrreadng = runiform()
replace vehodmtrreadng = round(vehodmtrreadng,0.01)*100000
*replace vehodmtrreadng = . if vehodmtrreadng>80000

label var vehodmtrreadng "odometer reading"
tostring vehodmtrreadng, replace

gen vehsalesprice = runiform()
replace vehsalesprice=round(vehsalesprice,0.00001)*1000000
label var vehsalesprice "sales price"

gen salestaxpaid = round(0.065*vehsalesprice)
label var salestaxpaid "sales tax (usually 6.5%, not in DMV codebook)"

gen vehsolddateyr = 2007
label var vehsolddateyr "Vehicle Sale Year"

gen vehsolddatemo = runiformint(1,3)
label var vehsolddatemo "Vehicle Sale Month"

gen vehsolddatedom = runiformint(1,29)
label var vehsolddatedom "Vehicle Sale Day of Month"

gen dieselindi  = .
label var dieselindi "Diesel Indicator"

gen flooddmgeindi = . 
label var flooddmgeindi "flood damage indicator"

gen govtowndindi = . 
label var govtowndindi "government owned indicator"

gen ttlhotckindi =.
label var ttlhotckindi "title hot check indicator"

gen othrstatecntry = . 
label var othrstatecntry "Other State Country"

gen surrttldateyr = . 
label var surrttldateyr "surrendered title year"

gen surrttldatemo = . 
label var surrttldatemo "surrendered title month"

gen surrttldatedom = . 
label var surrttldatedom "surrendered title day of month"

gen ttlvehlocst = ""
label var ttlvehlocst "title vehicle location street 1"

gen ttlvehlocst2 = ""
label var ttlvehlocst2 "title vehicle location street 2"

gen ttlvehloccity = ""
label var ttlvehloccity "title vehicle location city"

gen ttlvehlocstate = ""
label var ttlvehlocstate "title vehicle location state"

gen ttlvehloczpcd = ""
label var ttlvehloczpcd "title vehicle location zip code"

gen ttlvehloczpcdp4 = ""
label var ttlvehloczpcdp4 "title vehicle location zip code plus 4"

gen transcd = ""
label var transcd "transaction code"

gen rescomptcntyno = runiformint(1,254)
label var rescomptcntyno "resident comptroller county number"

gen regeffdateyr = runiformint(2004,2010)
label var regeffdateyr "registration effective year"
label var regeffdateyr "registration effective year"

gen regeffdatemo = runiformint(1,12)
label var regeffdatemo "registration effective month"

gen regeffdatedom = runiformint(1,29)
label var regeffdatedom "registration effective day of month"

ralpha prevpltno, upperonly l(7)
label var prevpltno "Previous Plate Number"

gen prevexpyr = runiformint(2004,2010)
label var prevexpyr "previous expiration year"

gen prevexpmo = runiformint(1,12)
label var prevexpmo "previous expiration month"

ralpha n1, upperonly l(6)
ralpha n2, upperonly l(7)
gen ownrttlname = n1 + " " + n2
label var ownrttlname "owner title name 1"
drop n1 n2

gen ownrttlname2 = "" 
label var ownrttlname2 "owner title name 2"

ralpha st, upperonly l(6) 
gen ownrst = st + " AVE" 
label var ownrst "owner street 1"
drop st

gen ownrst2 = ""
label var ownrst2 "owner street 2"

gen ownrcity = "SAN ANTONIO" if _n<=30
replace ownrcity = "COLLEGE STATION" if _n>30 & _n<=60
replace ownrcity = "DALLAS" if _n>30 & _n>60
label var ownrcity "owner city"

gen ownrstate = "TX" 
label var ownrstate "previous owner state"

gen ownrzpcd = 78109 if ownrcity == "SAN ANTONIO"
replace ownrzpcd = 77802 if ownrcity == "COLLEGE STATION"
replace ownrzpcd = 75098 if ownrcity == "DALLAS"
label var ownrzpcd "owner zip code"

gen ownrzpcdp4 = ""
label var ownrzpcdp4 "owner zip code plus 4"

gen ownrcntry = ""
label var ownrcntry "owner country"

/* OTHER VARIABLES THAT ARE IN DMV DATA BUT NOT IN SYNTHESIZED DATA

-	mil (geocode match number 1)
-	nmil (geocode match number 2)
-	vehton (vehicle tonnage)
-	trlrtype (trailer type)
-	vehbdyvin (vehicle body vin)
-	vehlngth (vehicle length)
-	vehodmtrbrnd (vehicle odometer brand)
-	vehemptywt (vehicle empty weight)
-	addllienrecrdindi (Additional lien recorded indicator)
-	agncyloandindi (agency loaned indicator)
-	dotstndrdsindi (department of transportation standards indicator)
-	dpsstlnindi (department of public safety stolen indicator)
-	exmptindi (exempt indicator)
-	fxdwtindi (fixed weight indicator)
-	inspectnwaivedindi (inspection waived indicator)
-	jnkindi (junk indicator)
-	prmtreqrdindi (permit required indicator)
-	priorccosissueindi (prior certified copy of original Texas Title Issue Indicator)
-	recondcd (reconditioned code)
-	recontindi (reconstructed indicator)
-	survshprightsindi (survivorship rights indicator)
-	ttlrevokdindi (title revoked indicator)
-	vinerrindi (VIN Error Indicator)
-	legalrestrntno (legal restraint number)
-	ccoissuedateyr (certified copy of original issue year,month, day of month)
-	salvstatecntry (Salvage State Country)
-	privacyoptcd (privacy option code)
-	certfdlienhldrindi1 (certified lienholder indicator of 1st lienholder)
-	lienholdrno1 (lienholder number of first lienholder)
-	liendate1yr (lien year for first lienholder)
-	liendate1mo 
-	liendate1dom
-	certfdlienholdrindi2 (certified lienholder indicator of second lienholder)
-	lienhldrno2 (lienholder number of second lienholder)
-	liendate2yr (lien year for second lienholder)
-	liendate2mo (lien month for second lienholder)
-	liendate2dom
-	certfdlienholdrindi3 (certified lienholder indicator of third lienholder)
-	lienhldrno3 (lienholder number of third lienholder)
-	liendate3yr (lien year for third lienholder)
-	liendate3mo (lien month for third lienholder)
-	liendate2dom
*/

sort dmvid
save "C:/Users/carli/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataRAW/dmv2007_1", replace
