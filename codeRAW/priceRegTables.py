# Author: Carla
# Date created: 2/14/20
# Description: This script merges together dmv individual records and eitc
# variables needed to run specification 1 from our paper.

import pandas as pd
# ==============================================================================
# GLOBALS
# ==============================================================================

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"
    # dataTEMP = "E:/TxDMV Registrations data/TEMP/"

    dataCLEAN = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataCLEAN/"
    # dataCLEAN = "E:/TxDMV Registrations data/CLEAN/"
    
    begin_year = 2006
    end_year = 2007

    chunk_size = 100000

    # ==================Other variables (don't need to change)==================
    eitc_zcta_name = dataTEMP + 'zcta_data/' + 'eitcVars_zcta_allyrs.csv'
    eitc_cty_name = dataTEMP + 'cty_data/' + 'eitcVars_cty_allyrs.csv'
    dmv_records_name = dataTEMP + 'dmv_records/' + \
                       'dmv_recordlvlvars_{}_{}.csv'.\
                           format(begin_year, end_year)

    output_filename = dataCLEAN + \
                      'priceregtables_{}_{}.csv'.\
                          format(begin_year, end_year)


    # ==========================================================================
    createTableData(eitc_zcta_name, eitc_cty_name,
                    dmv_records_name, chunk_size,
                    begin_year, end_year,
                    output_filename)


    return 0

# ==============================================================================
# Sub programs
# ==============================================================================
def createTableData(eitc_z_filename, eitc_c_filename, dmv_filename,
                    chunksize, begin_year, end_year, output_filename):

    eitc_z_df, eitc_c_df = loadEITCData(eitc_z_filename, eitc_c_filename,
                                        begin_year, end_year)
    dmv_iterator = loadDMVIterator(dmv_filename, chunksize)

    processDMVAndWrite(eitc_z_df, eitc_c_df, dmv_iterator, output_filename)



def loadEITCData(eitc_z_filename, eitc_c_filename, begin_year, end_year):
    eitc_z_vars = chooseEITCVars('zcta')
    eitc_c_vars = chooseEITCVars('ctyfips')

    eitc_z_df = pd.read_csv(eitc_z_filename, usecols=eitc_z_vars)
    eitc_z_df = subsetYears(eitc_z_df, begin_year, end_year)

    eitc_c_df = pd.read_csv(eitc_c_filename, usecols=eitc_c_vars)
    eitc_c_df = subsetYears(eitc_c_df, begin_year, end_year)

    # TODO: Load dealership concentration variable
    return eitc_z_df, eitc_c_df


def chooseEITCVars(geo_key):
    col_names = [geo_key, 'year', 'shareEITC', 'avgeitc_rtrn', 'eitcdollars_percap',
                 'eitcdollars_perhh', 'eitctakeup_rate', 'shareXdollars_perhh',
                 'share_pcinc_eitc', 'share_hhinc_eitc']
    return col_names

def subsetYears(df, begin_year, end_year):
    df = df.loc[(df['year'] >= begin_year) & (df['year'] <= end_year)]
    return df

# TODO: Write function that loads dealership concetration variable


def loadDMVIterator(dmv_name, chunk_size):
    dmv_vars = chooseDMVVars()
    file_iterator = pd.read_csv(dmv_name, usecols=dmv_vars,
                                  chunksize=chunk_size, sep=',')
    return file_iterator


def chooseDMVVars():
    col_names = ['dmvid', 'hhid', 'vehodmtrreadng', 'miles2', 'vehmk', 'make',
                 'vehmodl', 'model', 'vehmodlyr', 'price', 'tax',
                 'vehsolddatemo', 'vehsolddateyr', 'ownrshpevidcd',
                 'sale_wk_of_yr', 'sale_wkdy',
                 'lnprice', 'lntax',
                 'zero_price', 'miss_title',
                 'zcta', 'ctyfips']
    return col_names



def processDMVAndWrite(eitc_z_df, eitc_c_df, dmv_iterator, output_filename):

    i = 1
    for chunk in dmv_iterator:
        print('Loop {}'.format(i))

        z_new_names, c_new_names, dmv_new_names = renameVars()
        merged_chunk = mergeAndRenameChunk(chunk, eitc_z_df, eitc_c_df,
                                   z_new_names, c_new_names, dmv_new_names)
        if i == 1:
            createCSVFileHeader(merged_chunk, output_filename)

        with open(output_filename, mode='a') as f:
            merged_chunk.to_csv(f, mode='a', header=False,
                                 index=False, encoding='utf-8')

        i = i + 1



def createCSVFileHeader(df, output_filename):
    """
    Creates a csv file with column names that loaded with file_iterator.   """

    header = pd.DataFrame(columns=df.columns)
    header.to_csv(output_filename, sep=',', index=False)


def mergeAndRenameChunk(chunk, eitc_z_df, eitc_c_df,
                        eitc_z_dict, eitc_c_dict, dmv_dict):

    chunk = chunk.merge(eitc_z_df,
                        left_on=['zcta', 'vehsolddateyr'],
                        right_on=['zcta', 'year'],
                        how='inner')
    chunk.drop(columns='year', inplace=True)
    chunk.rename(columns=eitc_z_dict, inplace=True)


    chunk = chunk.merge(eitc_c_df,
                        left_on=['ctyfips', 'vehsolddateyr'],
                        right_on=['ctyfips', 'year'])
    chunk.drop(columns='vehsolddateyr', inplace=True)
    chunk.rename(columns=eitc_c_dict, inplace=True)
    chunk.rename(columns=dmv_dict, inplace=True)

    # TODO: merge dealership concentration variable here
    return chunk



def renameVars():
    # TODO: Pass column names and rename them. Pass this to createCSVFileHeader
    eitc_z_vars = chooseEITCVars('zcta')
    eitc_c_vars = chooseEITCVars('ctyfips')

    eitc_z_new_names = ['z_' + name for name in eitc_z_vars[2:]]
    eitc_c_new_names = ['c_' + name for name in eitc_c_vars[2:]]

    z_dict = dict(zip(eitc_z_vars[2:], eitc_z_new_names))
    c_dict = dict(zip(eitc_c_vars[2:], eitc_c_new_names))

    dmv_dict = {'vehsolddatemo': 'month', 'vehodmtrreadng': 'miles'}

    return z_dict, c_dict, dmv_dict








# ==============================================================================
# Execute
# ==============================================================================
main_program()
