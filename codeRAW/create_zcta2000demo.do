*Author: Carla
*Date Feb 10, 2020
*Description: Getting population numbers for zcta 2000

global dataRAW  "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataRAW\"
global dataTEMP "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataTEMP\"

insheet using "$dataRAW\zcta_census\nhgis0024_ds146_2000_zcta.csv", clear names
keep zctaa fl5001 fnh001

destring zctaa, replace force
drop if mi(zctaa)

rename zctaa zcta
rename fl5001 zcta_pop2000
rename fnh001 zcta_totalhh2000

tempfile pop2000
save `pop2000'



insheet using "$dataRAW\zcta_census\nhgis0024_ds151_2000_zcta.csv", clear names
keep zctaa gmy001 gnw001

destring zctaa, replace force
drop if mi(zcta)
rename zctaa zcta
rename gmy001 zcta_medhhinc2000
rename gnw001 zcta_percapinc2000

*Getting things in 2010 dollars (using pce_inflationyr_all.dta)
gen zcta_medhhinc2000adj = zcta_medhhinc2000 * 1.21781
gen zcta_percapinc2000adj = zcta_percapinc2000 * 1.21781

tempfile inc2000
save `inc2000'


import delimited "$dataRAW\crosswalks\zip_zcta_cty_xwalk_2015.csv", clear
merge m:1 zcta using `pop2000', nogen keep(3)
collapse (first) zcta_* ctyfips, by(zcta)
merge 1:1 zcta using `inc2000', nogen keep(3)

order zcta ctyfips
export delimited "$dataTEMP\zcta_data\zcta2000demo.csv", replace



