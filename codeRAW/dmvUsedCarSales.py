# Author: Carla
# Date created: 1/31/2020
# Description: Creates weekly and monthly used car sales at the zipcode
# and county levels from dmv record level data.


import pandas as pd
import numpy as np

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================
    #dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"
    dataTEMP = "E:/TxDMV Registrations data/TEMP/"

    # Other vars
    begin_year = 2004
    end_year = 2010

    # ===================Don't change these ====================================
    input_file = dataTEMP + 'dmv_records/' + \
                 'dmv_recordlvlvars_{}_{}.csv'.format(begin_year,
                                                      end_year)


    geo_index = [('zcta', 'zcta'), ('cty', 'ctyfips')]
    time_index = [('wk', ['sale_wk_of_yr']),
                  ('mnth', ['vehsolddatemo', 'vehsolddateyr']),
                  ('yr', ['vehsolddateyr'])]

    # ==========================================================================
    # Load data frame
    vars = chooseVars()
    dmv_records = pd.read_csv(input_file, usecols=vars)

    for (geo_name, geo_var) in geo_index:
        for (time_name, time_var) in time_index:

            output_file_name = dataTEMP + \
            '{}_data/'.format(geo_name) + \
                'dmv_usdcarsales_{}{}_{}_{}.csv'.format(geo_name,
                                                    time_name,
                                                    begin_year,
                                                    end_year)
            createGeoTimeData(dmv_records,
                              time_level=time_var,
                              geo_level=geo_var,
                              output_file=output_file_name)
            print("Created {} {} level data".format(geo_name, time_name))



# ==============================================================================
# Sub programs
# ==============================================================================
def chooseVars():
    dmv_id = ['dmvid']
    geo_ids = ['ownrzpcd', 'zcta', 'ctyfips']
    time_vars = ['sale_wk_of_yr', 'vehsolddatemo', 'vehsolddateyr']

    col_names = dmv_id + geo_ids + time_vars
    return col_names

def createGeoTimeData(df, time_level, geo_level, output_file):
    used_car_sales_df = sumUsedCarSales(df, time_level, geo_level)
    used_car_sales_df['lnusedcarsales'] = np.log(used_car_sales_df['usedcarsales'])

    used_car_sales_df.to_csv(output_file, index=False)

def sumUsedCarSales(df, time_level, geo_level):
#     TODO: should still have crosswalks
    level_to_group = time_level + [geo_level]
    columns_to_grab = ['dmvid'] + level_to_group

    grouped =  df[columns_to_grab].\
        groupby(level_to_group, as_index=False).\
        count()

    grouped.rename(columns={'dmvid':'usedcarsales'}, inplace=True)

    return grouped


# TODO: Add function that sums up used car sales across individual sellers as well.

# ==============================================================================
# Execute
# ==============================================================================
main_program()