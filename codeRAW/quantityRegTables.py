# Author: Carla
# Date created: 2/10/20
# Description: This script merges together dmv used car sales and eitc
# variables needed to run specification 1 from our paper.

import pandas as pd
# ==============================================================================
# GLOBALS
# ==============================================================================

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"
    # dataTEMP = "E:/TxDMV Registrations data/TEMP/"

    dataCLEAN = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataCLEAN/"
    # dataCLEAN = "E:/TxDMV Registrations data/CLEAN/"

    begin_year = 2006
    end_year = 2007

    # ==================Other variables (don't need to change)===================
    geo_index = [('zcta', 'zcta'), ('cty', 'ctyfips')]
    time_index = ['mnth']



    # ==========================================================================
    for (geo_name, geo_var) in geo_index:
        for time_name in time_index:
            geo_file_path = dataTEMP + '{}_data/'.format(geo_name)
            used_car_filename, eitc_filename = inputFileNames(geo_name,
                                                              time_name,
                                                              begin_year,
                                                              end_year,
                                                              geo_file_path)
            output_filename = dataCLEAN + \
                              'qtyregtables_{}_{}_{}_{}.csv'.\
                                  format(geo_name, time_name,
                                         begin_year, end_year)

            createTableData(geo_var, used_car_filename, eitc_filename,
                            output_filename)


    return 0

# ==============================================================================
# Sub programs
# ==============================================================================
def inputFileNames(geo_name, time_period, begin_year, end_year, geo_path):
    used_car_sales = geo_path + \
                     'dmv_usdcarsales_{}{}_{}_{}.csv'.\
                         format(geo_name, time_period, begin_year, end_year)
    eitc = geo_path +\
                'eitcVars_{}_allyrs.csv'.\
                    format(geo_name)
    # TODO: Add dealership concentration file name
    return used_car_sales, eitc


def createTableData(geo_key, used_car_filename, eitc_filename, output_filename):
    used_car_df, eitc_df = loadData(geo_key, used_car_filename, eitc_filename)
    merged_df = mergeData(geo_key, used_car_df, eitc_df)
    renameVars(merged_df)
    outputMergedDf(merged_df, output_filename)


def loadData(geo_key, used_car_filename, eitc_filename):
    used_car_df = pd.read_csv(used_car_filename)

    eitc_vars = chooseEITCVars(geo_key)
    eitc_df = pd.read_csv(eitc_filename, usecols=eitc_vars)

    # TODO: Load dealership concentration variable
    return used_car_df, eitc_df


def chooseEITCVars(geo_key):
    col_names = [geo_key, 'year', 'shareEITC', 'avgeitc_rtrn', 'eitcdollars_percap',
                 'eitcdollars_perhh', 'eitctakeup_rate', 'shareXdollars_perhh',
                 'share_pcinc_eitc', 'share_hhinc_eitc']
    return col_names


def mergeData(geo_key, used_car_df, eitc_df):
    merged_df = used_car_df.merge(eitc_df,
                                  left_on=[geo_key, 'vehsolddateyr'],
                                  right_on=[geo_key, 'year'],
                                  how='inner')
    merged_df.drop(columns='vehsolddateyr', inplace=True)
    # TODO: merge dealership concentration
    return merged_df

def renameVars(df):
    df.rename(columns={'vehsolddatemo': 'mnth'}, inplace=True)


def outputMergedDf(merged_df, output_filename):
    merged_df.to_csv(output_filename, index=False, sep=',')

# ==============================================================================
# Execute
# ==============================================================================
main_program()
