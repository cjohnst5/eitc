This file contains a description of each of the files in codeRAW and the order they should be run.
It also contains notes on how we constricted our dataset of DMV records.  


===================Overall structure of codeRAW======================
*Record level or geographic level data sets that are ready to be merged to other datasets
should be stored in dataTEMP


**** = written and debugged on local
# = Debugged with Josh

#****1. extract.py: Pull out data we want from DMV records

#****1.2 zip_zcta_cty crosswalks (do file)

****1.2.1 zcta_2010 variables (do file, using census data)

****1.3 zcta_2004 variables: income per capita and per household. 
    - crosswalk these with your 2015 crosswalk. 

#****2. dmvRecordLevelvars.py: Makes the mock dataset I have in excel spreadsheet. Saves this new data in a different .csv file. 

2.1 dmvConstrictRecordLevel: gets rid of missing variables and constricts models and model years

#****3. dmvUsedCarSalesNumbers.py (3 hours): Makes the weekly and monthly used car sales numbers for zctas
   and counties. Includes time variables as well (week, month, year) 

4. dmvDealershipConcentration.py: defines dealership concentration at a yearly level. 
    Different functions for different ways we want to measure this. 

****5. EITCVars.py (4 hours): Creates eitc data at the zcta and county levels

#****6. quantityRegTables.py (3 hours): Creates the zipcode and county level monthly tables we will use for spec 1 in STATA
6.2 Weekly level

#****7. binScatterTables.py. Creates the datasets needed for the bin scatter data
#****7.1 Quantity as lhs
    7.2 TODO: Price as lhs
#****8. priceRegTables.py (3 hours): Creates the individual records level table we will use in our analysis. 


13 hours until we get code for analysis sample. 


Future to-do's 
1. Don't export indices
2. For weekly and individual level datasets, we need event time dummies. 
3. Create a script that cleans missing variables from dmvRecordLevelVars (price = 0, make, model, or dates missing). This script will go between #2 and #3 above. 




===================Quick list of order to run files in codeRAW =====================
1. create_synthetic_dmyYYYY_N.do
2. create_zip_zcta_cty_crosswalks.do
3. create_zcta2000demo.do
4. create_zcta2010demo.do
5. extract.py
6. dmvRecordLevelVars.py
7. dmvConstrictRecordLevel.py (not yet functioning, but future data constrictions will go in here)
8. EITCvars.py 
9. dmvUsedCarSalesNumbers.py
10. quantityRegTables.py
11. priceRegTables.py 
12. binScatterTables.py


==================== Sample cleaning notes ==================================
1. Make sure the probability of missing a title and of having a zero price doesn't change in Feb, March. 
   1.1 If it doesn't change, then drop records that are missing title or have zero price (Do this in  dmvConstrictRecordLevel.py)   
2. Impute model from vehicle class and make if model is missing. (E.g. Toyota truck is now a model, Honda 4-door is now a model). 
   Do this in dmvConstrictRecordLevel.py. 




=========================================Extract sample notes ==========================================
""" 

Ways we need to constrict our sample 
1. Cars 
	- ownrshipevidcd (We dont want salvaged titles or repossesion) 
	- bndedttlcc (Could we have a missing ownrshipevidcd but have a bonded title?) TODO: comment
	  this in the code to possibly use it. For now we wont use it constrict our sample until we know more
	  what a bonded title is.
	- vehclasscd (vehicle class code). Restrict sample to pass, pass trck, trcks<=1	
2. Used cars 
	- vehodmtrreadng. Used cars = vehicles with 10,000 or more miles. 
3. Owner and buyer in the state of Texas
	- ownrstate. This is the state of the buyer. Needs to equal "TX"
	- prevownrstate. Also needs to equal "TX"
4. Car sale, rather than a registration due to a move or something else. 
	- Fined if sales price not w/in 30 days of registration price 
	- regeffdateyr/mo/dom. 
	- Subtract from vehsolddateyr = Year vehicle sold, vehsolddatemo = Month, vehsolddatedom = Day of month. 
	  Anything greater than 30 should be tossed out. 
5. Which buyer bought from which dealer(for k-means clustering in the future)
    - Need to grab owner and dealer names and addresses (listed below)
6. Other variables we will need for the specification.
	- ownrttlname (Im assuming this is the owner first name)
	- ownrttlname2 (Assuming owner last name)
	- ownrst (Owner house number)
	- ownrst2 (Owner street)
	- ownrcity (Owner city)
	- ownrstate (Owner state)
	- ownrzpcd (Zipcode)
	- ownrzpcd4 (zipcode + 4)
	- prevownrname (Previous owner name)
	- prevownrcity (Previous owner city)
	- prevownrstate (Previous owner state)
	- vehmk (vehicle make)
	- vehmodlyr (vehicle model year)
	- vehmodl (vehicle model)
	- vehsalesprice (sales price)
	- salestaxpaid (Usually 6.25 percent of sales price)
	- lien variables for 1, 2 and 3. 
	- certfdlenhldrindi1, 2, and 3. 
	- vin
"""

