#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
EITC Incidence in the Market for Large Durables
\end_layout

\begin_layout Standard
Consider the market for a large durable good, 
\begin_inset Formula $x$
\end_inset

, such as a household appliance in a static setting.
 Assume that in this market there is a continuum of potential consumer household
s of size 1, where each households's latent demand is defined by 
\begin_inset Formula 
\[
d_{i}^{*}(p_{i})=\begin{cases}
1 & if\text{ }P\leq p_{i}\\
0 & else
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $p_{i}$
\end_inset

 is the highest price household 
\begin_inset Formula $i$
\end_inset

 will pay for the good 
\begin_inset Formula $x$
\end_inset

.
 First, for simplicity, assume 
\begin_inset Formula $p_{i}\sim u[0,1]$
\end_inset

.
 Under a uniform distribution, aggregate latent demand for good 
\begin_inset Formula $x$
\end_inset

 will be 
\begin_inset Formula $D^{*}(P)=1-P$
\end_inset

.
 However suppose that a share of households, 
\begin_inset Formula $s$
\end_inset

, that are cash constrained and cannot purchase 
\begin_inset Formula $x$
\end_inset

, even at the price that they are willing to pay, 
\begin_inset Formula $p_{i}$
\end_inset

.
 Once again assume for simplicity that 
\begin_inset Formula $s_{i}\sim u[0,1]$
\end_inset

 and at least initially assume that 
\begin_inset Formula $s_{i}$
\end_inset

 and 
\begin_inset Formula $p_{i}$
\end_inset

 are uncorrelated.
 Given these cash constraints, realized demand becomes 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
d_{i}(p_{i},s_{i})=\begin{cases}
d_{i}^{*}(p_{i}) & if\text{ }s_{i}>s\\
0 & s_{i}\leq s
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
In aggregate, realized demand will then be 
\begin_inset Formula $D(P)=(1-P)(1-s)$
\end_inset

.
 Now suppose that everyone with 
\begin_inset Formula $si\leq s$
\end_inset

 is given a transfer of income that relaxes the constraint, such that 
\begin_inset Formula $d_{i}=d_{i}^{*}$
\end_inset

for all 
\begin_inset Formula $i$
\end_inset

.
 Because we have assumed 
\begin_inset Formula $s_{i}$
\end_inset

 and 
\begin_inset Formula $p_{i}$
\end_inset

 are uncorrelated, the demand curve will uniformly shift out from 
\begin_inset Formula $D$
\end_inset

 to 
\begin_inset Formula $D^{*}$
\end_inset

.
 This outward shift in demand will lead to a new market price, that will
 depend on the elasticity of supply of good 
\begin_inset Formula $x$
\end_inset

.
 If 
\begin_inset Formula $x$
\end_inset

 is supplied more elastically (
\begin_inset Formula $S_{1}$
\end_inset

) then the new market price will be 
\begin_inset Formula $P_{1}$
\end_inset

.
 If the supply of 
\begin_inset Formula $x$
\end_inset

 is relatively more inelastic (
\begin_inset Formula $S_{2}$
\end_inset

) then the new market price will be 
\begin_inset Formula $P_{2}$
\end_inset

.
 In each case we can calculate the incidence of the transfer, by comparing
 the original price, to the new price, and to the price that would have
 been observed in the original state if demand was at the new level of demand.
 
\end_layout

\begin_layout Standard
The original inverse demand function is 
\begin_inset Formula $P=1-\frac{D}{1-s}$
\end_inset

.
 In either setting, the consumer incidence will be 
\begin_inset Formula 
\[
share_{c}=\frac{P_{0}-p_{0i}}{P_{i}-p_{0i}}=\frac{P_{0}-(1-\frac{X_{i}}{1-s})}{P_{i}-(1-\frac{X_{i}}{1-s})}
\]

\end_inset


\end_layout

\begin_layout Standard
Note that the consumer share of the transfer corresponds to the blue arrows
 in the figure, and the producer's share will be 
\begin_inset Formula $share_{p}=1-share_{c}$
\end_inset

, and corresponds to the orange arrows.
 If the supply curve is more elastic than the demand curve, a larger share
 of the transfer will be retained by the consumer.
 However, if the supply curve is more inelastic, a larger share of the transfer
 will be passed through to the producers.
 As we do not know the relative elasticity of the demand and supply curve,
 this remains an empirical question.
\end_layout

\begin_layout Standard
Figure 1.
 Market for durable good 
\begin_inset Formula $x$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename ../figures/supply_demand.png
	scale 50

\end_inset


\end_layout

\end_body
\end_document
