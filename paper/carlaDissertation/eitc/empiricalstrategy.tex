\section{Empirical Strategy: Difference-in-differences}\label{section:eitcempirics}
	This section describes our empirical strategy to recover causal estimates of the effect of EITC returns on quantities and prices in the used vehicle market. We use a difference-in-differences design, taking advantage of the regional and yearly variation in EITC population shares across Texas and our knowledge of the EITC return schedule. Because most EITC returns are issued in February and March, we can surmise that zipcodes with large populations of EITC recipients will be ``treated'' by the EITC return in February and March. The coefficients of interest from specification \ref{eq:quantity} below will measure the effect of EITC returns on a zipcode's used car sales.

    \begin{equation}\label{eq:quantity}
    \begin{split}
     ln(\mbox{sales}_{z, t}) &= \beta_0 + \sum_{m \in \mathbf{M}} \left(\beta_m * 1\{month = m\} * \mbox{shareEITC}_{g,yr(t)}\right) \\ 
                        &+  \beta_2*\mbox{shareEITC}_{z,yr(t)} \\
                         & + \Gamma_{t}  + \Gamma_{m(t)} + \Gamma_{yr(t)} + \Gamma_z + \epsilon_{z,t}
    \end{split}
    \end{equation}
    
    In this specification, $ln(\mbox{sales}_{z, t})$ is the log of used cars sales in zipcode $z$ in month-year t. The variable $\mbox{shareEITC}_{z, yr(t)}$ is the share of the population claiming the EITC for zipcode $z$ in year $yr(t)$. This specification includes time-period ($\Gamma_t$), month ($\Gamma_{m(t)}$), and year fixed effects ($\Gamma_{yr(t)}$), as well as zipcode fixed effects ($\Gamma_z$).  The main interaction terms include month-share interactions for every month except July, which we use as our baseline. The set of months excluding July form the set of months $\mathbf{M}$. The coefficients of interest are the parameters estimated from the interaction of the EITC population share with the February and March month dummies, $\beta_{m=Feb}$ and $\beta_{m=Mar}$.
    These coefficients can be interpreted as a lower bound on the shift in the demand curve from our model in section 2, under the stringent assumption that the supply curve remains unchanged.\footnote{It is a lower bound because our model assumes a lump sum endowment lifts all cash-constrained individuals onto their true demand curve, while the actual EITC only unconstrains some previously cash-constrained individuals.} 
    
    
    Specification \ref{eq:prices} below presents our empirical design to recover the changes in prices caused by EITC returns. Our design is similar to the specification \ref{eq:quantity}, with individual log sales price as the outcome variable, instead of zipcode-level sales numbers. 
    
        \begin{equation}\label{eq:prices}
        \begin{split}
            ln(\mbox{price}_{i}) &= \beta_0 + \sum_{m \in \mathbf{M}} \left(\beta_m * 1\{month = m\} * \mbox{shareEITC}_{g,yr(t)}\right) \\
                    & +  \beta_2*\mbox{shareEITC}_{z,yr(t)} \\
                    & + \Gamma_{t}  + \Gamma_{m(t)} + \Gamma_{yr(t)} + \Gamma_z + \Gamma_c + \epsilon_{i}
        \end{split}
        \end{equation}

    We use the individual record level data from the Texas DMV to obtain vehicle prices and characteristics and zipcode-level EITC variation. As above, the coefficients of interest are the $\beta_{m=Feb}$ and $\beta_{m=Mar}$. We include additional controls for used car characteristics, denoted in $\Gamma_c$. These controls include vehicle model and model year interaction terms, as well as quadratic controls for odometer mileage. 