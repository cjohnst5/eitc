# Created by Carla Johnston
# January 22, 2020
# The file creates dmv individual record level variables. 


import pandas as pd
import numpy as np

# ==============================================================================
# Main program function
# ==============================================================================
# TODO: Add timing


def recordLevelVarsMain():
    # TODO: Josh, change dataRAW to the folder which contains the crosswalk folder
    # TODO: Josh, dataTEMP contains the folder with dmv records extract
    # TODO: Josh, begin_year is the first year in the dmv_extract file name
    # TODO: Josh, end_year is the last year in the dmv_extract file name
    # TODO: Josh, input_file_name is the filepath+name of dmv_extract csv
    # TODO: Josh, output_file_name is filepath+name of where to save this csv file
    # TODO: Josh, crosswalk_file_name is filepath+name of crosswalk file

    # ==================Variables to set =======================================
    dataRAW = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataRAW/"
    # dataRAW = "E:/TxDMV Registrations data/registrations/"

    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"
    # dataTEMP = "E:/TxDMV Registrations data/TEMP/"
    
    begin_year = 2006
    end_year = 2007

    # ==================Variables you don't need to change======================
    input_file_name = dataTEMP + 'dmv_records/' + \
                      'dmv_extract_{}_{}.csv'.format(begin_year, end_year)
    output_file_name = dataTEMP + 'dmv_records/' + \
                       'dmv_recordlvlvars_{}_{}.csv'.format(begin_year,
                                                            end_year)
    crosswalk_file_name = dataRAW + 'crosswalks/zip_zcta_cty_xwalk_2015.csv'

    price_inflator = dataRAW + 'priceinflator/pce_inflationyr_all.dta'

    # ==========================================================================
    # TODO: Think about data types here or iterating over chunks if code is slow
    dmv_records = pd.read_csv(input_file_name)

    # Variable creation
    dmv_records = priceInflationAdj(price_inflator, dmv_records)
    dmv_records['sale_wk_of_yr'] = saleWeekOfYr(dmv_records)
    dmv_records['sale_wkdy'] = dayOfWeek(dmv_records)

    dmv_records['lnprice'] = np.log(dmv_records['price'])
    dmv_records['lntax'] = np.log(dmv_records['tax'])

    dmv_records['make'] = pd.factorize(dmv_records['vehmk'])[0]
    dmv_records['model'] = pd.factorize(dmv_records['vehmodl'])[0]
    dmv_records['miles2'] = dmv_records['vehodmtrreadng']*dmv_records['vehodmtrreadng']

    dmv_records['zero_price'] = 0
    dmv_records.loc[dmv_records['vehsalesprice'] == 0, 'zero_price'] = 1

    dmv_records['miss_title'] = 0
    dmv_records.loc[dmv_records['ownrshpevidcd'] == 'Missing', 'miss_title'] = 1


    dmv_records = mergeCrosswalks(dmv_records, crosswalk_file_name)

    dmv_records.to_csv(output_file_name, sep=',', index=False)


# ==============================================================================
# Sub programs
# ==============================================================================
def saleWeekOfYr(df):
    sale_date = createDate(df, 'vehsolddateyr', 'vehsolddatemo',
                           'vehsolddatedom')
    return sale_date.dt.to_period('W')

def dayOfWeek(df):
    sale_date = createDate(df, 'vehsolddateyr', 'vehsolddatemo',
                           'vehsolddatedom')
    return sale_date.dt.dayofweek


def createDate(data_frame, year_var, month_var, day_var):
    """
    Creates a datetime variable using year, month, and day vars
    :param data_frame:
    :param year_var: string. Name of year variable. Year var can be float or int.
    :param month_var: string. "" ""
    :param day_var: string. "" ""
    :return: pandas datetime variable. E.g. 12/1/2019
    """
    return pd.to_datetime(
        {'year': data_frame[year_var].astype('int32'),
         'month': data_frame[month_var].astype('int32'),
         'day': data_frame[day_var].astype('int32')}, errors='coerce')


def mergeCrosswalks(df, crosswalk_file):
    zip_zcta_cty_xwalk = pd.read_csv(crosswalk_file)

    merged = df.merge(zip_zcta_cty_xwalk, how='outer',
                      left_on='ownrzpcd',
                      right_on='zipcode',
                      indicator=True)
    print("Merging zcta crosswalk")
    print(merged['_merge'].groupby(merged['_merge']).size())
    merged = merged.loc[merged['_merge'] == 'both', :]
    merged.drop(['_merge', 'zipcode'], axis=1, inplace=True)

    return merged


def priceInflationAdj(inflator_filename, df):

    inflator_df = pd.read_stata(inflator_filename)
    yr_range = (inflator_df['year'] >=2004) & (inflator_df['year']<=2010)
    inflator_df = inflator_df.loc[yr_range, :]

    df = df.merge(inflator_df, how='inner',
                            left_on = 'vehsolddateyr',
                            right_on = 'year')

    df['price'] = df['vehsalesprice']*df['pce_inflator']
    df['tax'] = df['salestaxpaid']*df['pce_inflator']

    df.drop(columns='pce_inflator', inplace=True)
    return df


def event_dummies(df):
    #     TODO: Fill this function in once you have the EITC schedules from Riley
    return 0


def individualSellers():
    """
    Equals 1 if the seller was an individual, as opposed to a dealership
    TODO: Ask Josh to look at the prevownrname variable to see if he can
    come up with a good way to determine individual sellers.
    
    """
    # TODO: fill this function in on the second round of analysis
    return 0


# ==============================================================================
# Execute
# ==============================================================================
recordLevelVarsMain()
