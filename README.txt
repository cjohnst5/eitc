Author: Carla Johnston
Date: November 12, 2019
Subject: Project directory set up



The folders containing current data, code, and output for the project all have name endings of "RAW" or "CLEAN."

The workflow of the project can be generally described by going from "RAW" to "CLEAN" folders. 

The dataRAW folder contains any type of
data that is not ready to be used in data analysis. These include the synthetic Texas DMV data set, excel
sheets from the brookings EITC take up rates. This folder also includes datsets that have been cleaned somewhat, 
or do not need cleaning, but need to be merged to another dataset to be used in analysis. 

The codeRAW folder contains code that cleans the raw data, merges datasets together, and saves the clean, merged datasets in dataCLEAN. 

The dataCLEAN folder contains any data that was created/cleaned by code files in codeRAW. These datasets should not
need to be manipulated any further to be used in analysis (regressions, figures, e.g.). They are merged, cleaned, and include all relevant variables
and variable labels. 

The codeCLEAN folder contains code files that analyze the data found in dataCLEAN. Some example of analysis include
regressions and plots. 

codeCleanOUTPUT folder contains any figures, coefficients, latex tables, log files, or other output generated
by code files contained in the codeCLEAN folder. 



Dividing the data and code into the RAW and CLEAN groups allows us to easily
track changes in our results (did our results change because we changed the data or changed the regression controls?)
It also saves us a headache when we realize we need to merge additional datasets
on to our main analysis data or create new variables. If the data creation and data analysis
is interspersed, then it becomes unclear where in our code we should create a new variable or 
do a merge. 


The codeRAW, codeCLEAN, outputCodeCLEAN, and paper directories are all tracked in the git
repo housed at https://bitbucket.org/cjohnst5/eitc/src/master/.




