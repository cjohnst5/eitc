Author: Carla Johnston
Date: November 12, 2019
Subject: How to run scripts in the codeCLEAN folder


The codeCLEAN folder contains all analysis code. The code files use data found in dataCLEAN
as inputs and output results to codeCleanOUTPUT. 

Run the file mainDraft.do to run the majority of the data analysis programs in the right order. 

descriptiveStatistics.py and sumstats.do are stand alone scripts that provide basic sample summary stats I needed for my dissertation. 

