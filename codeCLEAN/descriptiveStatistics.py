# Author: Carla
# Date created: 2/24/2020
# Description: Creates descriptive statistic table

import pandas as pd

# ==============================================================================
# GLOBALS
# ==============================================================================

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataCLEAN = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataCLEAN/"
    outputCLEAN = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/outputCLEAN/"
    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"

    # dataCLEAN = "E:/TxDMV Registrations data/CLEAN/"


    # Other vars
    begin_year = 2006
    end_year = 2007


    # ==================Don't need to change these==============================
    dmv_records_path = dataCLEAN + 'priceregtables_{}_{}.csv'.\
                          format(begin_year, end_year)
    # zcta_data_path = dataCLEAN + 'qtyregtables_zcta_mnth_{}_{}.csv'.\
    #                       format(begin_year, end_year)
    zcta_data_path = dataTEMP + 'zcta_data/eitcVars_zcta_allyrs.csv'

    output_table = outputCLEAN + 'tables/descriptive_statistics_{}_{}.csv'.\
                          format(begin_year, end_year)

    # ==========================================================================
    car_avg, car_std = carLevelStats(dmv_records_path)

    zcta_vars = chooseZCTAVars()
    zcta_stats = zctaLevelStats(zcta_data_path, zcta_vars)

    stats = appendStats(car_avg, car_std, zcta_stats)
    stats.round(2).to_csv(output_table)

    return 0


# ==============================================================================
# Sub programs
# ==============================================================================
def carLevelStats(dmv_records_path):
    dmv_vars = ['miles', 'price', 'lnprice', 'make', 'model']
    dmv_df = pd.read_csv(dmv_records_path, usecols=dmv_vars)

    price_bool = dmv_df['price'] > 0
    model_bool = dmv_df['make'] != -1
    make_bool = dmv_df['model'] != -1

    non_miss_bool = price_bool & model_bool & make_bool
    dmv_df = dmv_df.loc[non_miss_bool, :]

    dmv_avg = dmv_df[['price', 'lnprice', 'miles']].mean()
    dmv_std = dmv_df[['price', 'lnprice', 'miles']].std()
    return dmv_avg, dmv_std

def chooseZCTAVars():
    eitc_vars = ['treturn', 'teic', 'teicam_adj', 'shareEITC',
        'avgeitc_rtrn', 'eitcdollars_percap', 'eitcdollars_perhh',
        'eitctakeup_rate', 'share_pcinc_eitc', 'share_hhinc_eitc',
        'zcta_medhhinc2000adj', 'zcta_percapinc2000adj']

    return eitc_vars

def zctaLevelStats(zcta_data_path, vars):
    zcta_df = pd.read_csv(zcta_data_path)

    zcta_avg = zcta_df[vars].mean()
    zcta_p25 = zcta_df[vars].quantile(.25)
    zcta_p75 = zcta_df[vars].quantile(.75)
    zcta_med = zcta_df[vars].median()
    zcta_std = zcta_df[vars].std()
    zcta_max = zcta_df[vars].max()
    zcta_min = zcta_df[vars].min()

    zcta_stats = pd.DataFrame({'mean': zcta_avg,
                               'p25': zcta_p25,
                               'p75': zcta_p75,
                               'median': zcta_med,
                               'std': zcta_std,
                               'max': zcta_max,
                               'min': zcta_min})


    return zcta_stats


def appendStats(dmv_avg, dmv_std, zcta_stats):
    # dmv_stats = pd.DataFrame({'mean': dmv_avg, 'std': dmv_std})


    # stats = dmv_stats.append(zcta_stats)

    return zcta_stats



# ==============================================================================
# Execute
# ==============================================================================
main_program()
