# Created by Carla Johnston
# December 10, 2019
# The file extracts observations relevant to our analysis from the 
# dmvYYYY_NN.dta files

# This code focuses on getting the observations we want in our sample. It also
# pulls out only the variables we need for our regressions.


# INSTRUCTIONS FOR JOSH (marked as TODO: Josh in code)
# 1. Define file paths and names
# 2. Define number of .dta observations to read into Python at a time
# 3. Change days between sale and registration to be <=30
# 4. Run script!



import pandas as pd
import time

lien = 'False'


# ==============================================================================
# Main program function
# ==============================================================================
def extract_main_program():
    # ==================Variables you need to set ==============================
    # TODO: Josh, you'll need to dataRAW to be the file path to the .dta files
    # TODO: Josh, set dataTEMP to the folder path of the appended data csv file
    # TODO: Josh, set the number of rows to read into Python at a time (~100000)
    # TODO: Josh, set number of days before late registration fee if it's not 30
    # TODO: JOsh, fileTEMP is the name of csv with appended data. Don't need to change.

    # Define paths
    dataRAW = "C:/Users/AdamB/Desktop/TEMP/"
    dataTEMP = "C:/Users/AdamB/Desktop/TEMP/"

    dataRAW = "C:/Users/carli/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataRAW/"
    dataTEMP = "C:/Users/carli/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"

    # Define other vars
    num_of_rows = 10
    legal_days_btw_sale_reg = 30
    year_index = [2006, 2007]
    n_index = [1]

    # If we are doing all years and all .dta files
    # year_index = [2004, 2005, 2006, 2007, 2008, 2009, 2010]
    # n_index = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    fileTEMP = 'dmv_extract_{}_{}.csv'.format(year_index[0], year_index[-1])
    # ===========================================================================
    vars_to_extract = chooseVars()

    # Setting up the csv file extracted data will be written to
    createCSVFileHeader(column_names=vars_to_extract,
                        csv_path=dataTEMP + 'dmv_records/',
                        csv_name=fileTEMP)
    # Extracting the data
    for year in year_index:
        for n in n_index:
            t0 = time.time()
            fileRAW = 'dmv{}_{}.dta'.format(year, n)

            runFileExtraction(raw_file_path=dataRAW, raw_file_name=fileRAW,
                              column_names=vars_to_extract,
                              chunk_size=num_of_rows,
                              extract_file_path=dataTEMP + 'dmv_records/',
                              extract_file_name=fileTEMP,
                              late_days=legal_days_btw_sale_reg)
            t1 = time.time()
            total_time = t1 - t0
            print(
            'dmv{}_{}.dta extraction: {} secs'.format(year, n, total_time))


            # If we need to go back and extract more variables, we can repeat
            # runFileExtraction(), cleanFileExtraction(), and then
            # mergeFileExtraction().


# ==============================================================================
# Sub programs
# ==============================================================================
def chooseVars():
    """
    List of variable names to pull from the dmvYYY_N files
    :return: None
    """
    ID_VARS = ['dmvid', 'hhid']
    OWNER_VARS = ['ownrttlname', 'ownrttlname2', 'ownrst', 'ownrst2',
                  'ownrcity',
                  'ownrstate', 'ownrzpcd', 'ownrzpcdp4', 'prevownrname',
                  'prevownrcity',
                  'prevownrstate']
    VEHICLE_VARS = ['vin', 'vehclasscd', 'vehodmtrreadng', 'vehmk', 'vehmodl',
                    'vehmodlyr', 'vehsalesprice', 'salestaxpaid']
    DATE_VARS = ['regeffdatedom', 'regeffdatemo', 'regeffdateyr',
                 'vehsolddatedom',
                 'vehsolddatemo', 'vehsolddateyr']
    TITLE_VARS = ['bndedttlcd', 'ownrshpevidcd']
    LIEN_VARS = ['certfdlienhldrindi1', 'lienhldrno1', 'liendate1yr',
                 'liendate1mo',
                 'liendate1dom',
                 'certfdlienhldrindi2', 'lienhldrno2', 'liendate2yr',
                 'liendate2mo',
                 'liendate2dom',
                 'certfdlienhldrindi3', 'lienhldrno3', 'liendate3yr',
                 'liendate3mo',
                 'liendate3dom']
    # TODO: Josh, I didn't verify if lien vars were loading because the
    # TODO: synthetic dataset doesn't have them. The lien var names might
    # TODO: have typos.

    if lien == 'True':
        COL_NAMES = ID_VARS + OWNER_VARS + VEHICLE_VARS + DATE_VARS + TITLE_VARS \
                    + LIEN_VARS
    else:
        COL_NAMES = ID_VARS + OWNER_VARS + VEHICLE_VARS + DATE_VARS + TITLE_VARS

    return COL_NAMES


def createCSVFileHeader(column_names, csv_path, csv_name):
    """
    Creates a csv file with column names that loaded with file_iterator.
    """
    header = pd.DataFrame(columns=column_names)
    header.to_csv(csv_path + csv_name, sep=',', index=False)


def runFileExtraction(raw_file_path, raw_file_name, column_names, chunk_size,
                      extract_file_path, extract_file_name, late_days):
    """

    :param raw_file_path: file path to raw dmvYYYY_N.dta files
    :param raw_file_name: dmvYYYY_N.dta
    :param column_names: List of column names from dmvYYYY_N.dta
    :param chunk_size: Number of rows to load at a time from dmvYYY_N.dta
    :param extract_file_path: file path to store temporary .csv extract files
    :param extract_file_name: file name of .csv extract file
    :return: None
    """
    iterator = readFile(raw_file_path, raw_file_name, column_names, chunk_size)
    processFile(iterator, raw_file_name, extract_file_path, extract_file_name,
                late_days)


def readFile(file_path, file_name, column_names, chunk_size):
    """

    :param file_path: string
    :param file_name: string
    :param column_names: list of column names to load from .dta file
    :param chunk_size: number of rows to load at a time from .dta file
    :return: .dta file iterator. We can iterate through this object one chunk
    at a time.
    """
    data_file = file_path + file_name
    file_iterator = pd.read_stata(data_file, columns=column_names,
                                  chunksize=chunk_size)
    return file_iterator


def processFile(file_iterator, file_name, csv_path, csv_name, late_days):
    """

    :param file_iterator: .dta file iterator
    :param file_name: string. dmvYYYY_N.dta
    :param csv_path: File path to store csv extract
    :param csv_name: csv extract name
    :return: None
    """
    i = 1
    for chunk in file_iterator:
        print('{} loop {}'.format(file_name, i))
        i = i + 1
        chunk_extract = processChunk(chunk, late_days)
        # chunk_extract = chunk

    
        with open(csv_path + csv_name, mode='a') as f:
            chunk_extract.to_csv(f, mode='a', header=False, 
                index=False, encoding='utf-8')



def processChunk(chunk, late_days):
    """
    Pulls used car sales from each chunk of the .dta file
    :param chunk: A raw chunk of observations from dmvYYYY_N.dta file
    :return: Used car sale observations
    """

    cars = (chunk['vehclasscd'] == 'PASS') | \
           (chunk['vehclasscd'] == 'PASS TRK') | \
           (chunk['vehclasscd'] == 'TRK<=1')

    usedVehicle = (
    chunk['vehodmtrreadng'].replace('EXEMPT', '-9').astype('float') >= 10000)

    sellerInTX = (chunk['prevownrstate'] == 'TX')
    buyerInTX = (chunk['ownrstate'] == 'TX')

    sale_date = createDate(chunk, 'vehsolddateyr', 'vehsolddatemo',
                           'vehsolddatedom')
    reg_date = createDate(chunk, 'regeffdateyr', 'regeffdatemo',
                          'regeffdatedom')
    days_btw_reg_sale = (reg_date - sale_date).dt.days

    carSale = (days_btw_reg_sale < late_days) & (days_btw_reg_sale >= 0)

    conditions_bool = cars & usedVehicle & sellerInTX & buyerInTX & carSale

    processed_chunk = chunk.loc[conditions_bool]
    return processed_chunk


def createDate(data_frame, year_var, month_var, day_var):
    """
    Creates a datetime variable using year, month, and day vars
    :param data_frame:
    :param year_var: string. Name of year variable. Year var can be float or int.
    :param month_var: string. "" ""
    :param day_var: string. "" ""
    :return: pandas datetime variable. E.g. 12/1/2019
    """
    return pd.to_datetime(
        {'year': data_frame[year_var].astype('int32'),
         'month': data_frame[month_var].astype('int32'),
         'day': data_frame[day_var].astype('int32')}, errors='coerce')


def cleanFileExtraction():
    """
    Whatever cleaning Josh deems neccessary.
    """
    return 0


def mergeFileExtraction():
    """
    Merges file extractions if additional variables need to be extracted from
    .dta files.

    We don't need this function for now.
    """
    return 0


# ==============================================================================
# Execute
# ==============================================================================
extract_main_program()
