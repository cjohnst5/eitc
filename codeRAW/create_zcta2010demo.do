*Author: Carla
*Date Feb 4, 2020
*Description: Getting population numbers for zcta

global dataRAW  "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataRAW\"
global dataTEMP "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataTEMP\"

insheet using "$dataRAW\zcta_census\DEC_10_SF1_SF1DP1_with_ann.csv", clear names
drop if _n == 1
destring geoid2 hd01_s001 hd01_s150 hd01_s151 hd01_s152 hd01_s153 hd01_s154 hd01_s158, replace
rename geoid2 zcta

gen zcta_pop2010 = hd01_s001
gen zcta_totalhh2010 = hd01_s150
gen zcta_famhh2010 = hd01_s151
gen zcta_famkidshh2010 = hd01_s152
gen zcta_marrhh2010 = hd01_s153
gen zcta_marrkidshh2010 = hd01_s154
gen zcta_singlemomhh2010 = hd01_s158
keep zcta*


tempfile zcta2010
save `zcta2010'

import delimited "$dataRAW\crosswalks\zip_zcta_cty_xwalk_2015.csv", clear

merge m:1 zcta using `zcta2010', nogen keep(3)
collapse (first) zcta_* ctyfips, by(zcta)
order zcta ctyfips
export delimited "$dataTEMP\zcta_data\zcta2010demo.csv", replace

