# Author: Carla
# Date created: 2/27/2020
# Description: Gets rid of observations with missing data and constricts
# used cars records to certain models and model years
# Update: Josh is just doing this in the main.do file. So this code is not functional


import pandas as pd
# ==============================================================================
# GLOBALS
# ==============================================================================

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataTEMP = "C:/Users/carli/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"

    # Other vars
    begin_year = 2006
    end_year = 2007

    # ==================Variables you don't need to change======================
    input_file = dataTEMP + 'dmv_records/' + \
                       'dmv_recordlvlvars_{}_{}.csv'.format(begin_year,
                                                            end_year)


    # ==========================================================================
    records_df = pd.read_csv(input_file)



    return 0

# ==============================================================================
# Sub programs
# ==============================================================================
def cleanMissObs(records_df):
    return 0



def constrictModels(records_df):

    models = records_df[['vehmk', 'vehmodl', 'vin']].\
        groupby(['vehmk', 'vehmodl']).\
        agg({'vin': 'count'})
    models_pct =
    return 0

# ==============================================================================
# Execute
# ==============================================================================
main_program()