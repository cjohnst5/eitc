*CREATE DLR_TRANSACTIONS

clear all
set obs 10

gen v = runiform()
replace v = round(v, 0.01)
replace v = v*100000000000000000
format v %20.0g
label var v "vin number"
tostring v, generate(vin) format(%20.0f) 
drop v

gen fqvin = 1 
label var fqvin "freq of vin number"

gen prevownrname = "JAY BEE" if _n<=5
replace prevownrname = "KAY DEE" if _n>5
label var prevownrname "previous owner name"

gen prevownrcity = "DENTON" if _n<=5
replace prevownrcity = "HOUSTON" if _n>5
label var prevownrcity "previous owner city"

gen prevownrstate = "TX" 
label var prevownrstate "previous owner state"

gen vehbdytype = "Coupe" if _n<=2
replace vehbdytype = "Truck" if _n==3
replace vehbdytype = "SUV" if _n>=4
label var vehbdytype "vehicle body type"

gen vehmk = "HOND" if !mod(_n,2)
replace vehmk = "FORD" if mod(_n,2)
label var vehmk "vehicle make"

gen vehmodlyr = runiformint(2002,2006)
label var vehmodlyr "model year"

gen vehodmtrreadng = runiform()
replace vehodmtrreadng = round(vehodmtrreadng,0.01)*100000
label var vehodmtrreadng "odometer reading"

gen vehsalesprice = runiform()
replace vehsalesprice=round(vehsalesprice,0.00001)*1000000
label var vehsalesprice "sales price"

gen salestaxpaid = round(0.065*vehsalesprice)
label var salestaxpaid "sales tax"

gen m = runiformint(1,12)
gen d = runiformint(1,30)
gen y = runiformint(2006,2010)

gen saledate = mdy(m,d,y)
format saledate %tdMon_DD_CCYY 
label var saledate "sales date"

gen regdate = mdy(m,d,y)
format regdate %tdMon_DD_CCYY 
label var regdate "registration date"

save "/Users/joshuawitter/Dropbox/EITC_carprice/dataRAW/dlr_transactions"
