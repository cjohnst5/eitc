\documentclass[12pt]{article}
\usepackage[latin9]{inputenc}
\usepackage[letterpaper]{geometry}
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1.25in}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage[authoryear]{natbib}
\PassOptionsToPackage{normalem}{ulem}
\usepackage{ulem}
\usepackage[unicode=true,
 bookmarks=false,
 breaklinks=false,pdfborder={0 0 1},backref=section,colorlinks=false]
 {hyperref}
\hypersetup{
 colorlinks,linkcolor=red,urlcolor=blue,citecolor=red,pdftex}

\makeatletter
\@ifundefined{date}{}{\date{}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.


\usepackage{array}
\usepackage{threeparttable}

\usepackage[format=hang,font=normalsize,labelfont=bf]{caption}
\usepackage{amsthm}
\usepackage{float}
\usepackage{color}\usepackage[pdftex]{graphicx}

\theoremstyle{definition}
\newtheorem{theorem}{}\newtheorem{acknowledgement}[theorem]{}\newtheorem{algorithm}[theorem]{}\newtheorem{axiom}[theorem]{}\newtheorem{case}[theorem]{}\newtheorem{claim}[theorem]{}\newtheorem{conclusion}[theorem]{}\newtheorem{condition}[theorem]{}\newtheorem{conjecture}[theorem]{}\newtheorem{corollary}[theorem]{}\newtheorem{criterion}[theorem]{}\newtheorem{definition}[theorem]{}\newtheorem{derivation}{}\newtheorem{example}[theorem]{}\newtheorem{exercise}[theorem]{}\newtheorem{lemma}[theorem]{}\newtheorem{notation}[theorem]{}\newtheorem{problem}[theorem]{}\newtheorem{proposition}{}\newtheorem{remark}[theorem]{}\newtheorem{solution}[theorem]{}\newtheorem{summary}[theorem]{}\numberwithin{equation}{section}

\newcommand{\ve}{\varepsilon}
\renewcommand{\theenumi}{\Alph{enumi}}
\renewcommand{\theenumii}{\roman{enumii}}

\makeatother

\begin{document}

\title{Capturing Supply Changes in the Market for Used Vehicles}
\author{Riley Wilson}
\date{\today}
\maketitle

\section{Motivation}
We hypothesize that receipt of the EITC will shift out demand for low cost, used vehicles. In theory, this should lead to changes in both prices and quantities. If we assume the supply curve is fixed, we can interpret these equilibrium changes and back out how the ``incidence'' of the EITC ``subsidy'' is distributed across demand (recipients) and supply (firms and individuals that sell their cars.

However, if the supply curve is not fixed but also moves when people get their EITC disentangling price and quantity changes to interpret incidence is more difficult. To overcome this challenge, we propose to control or proxy for potential changes in the supply curve.

The supply of used vehicles will be directly related to the number and nature of the vehicles currently on the road. We plan to use the universe of car registrations in Texas to capture changes in the stock of cars available for trade in (and future supply). There are three proposed ways of capturing this.

\subsection{Strategy 1} 
For each make, model and year we will construct annual hazard rates to capture the probability that this particular style of car will still be functioning and on the road in any given year. As the probability of being on the road falls, it is likely that these cars are leaving the market and going to the junk yard or being used for scrap. For example, if we consider the number of 2001 Toyota Corollas on the road in 2006 to 2013 we might see the number falling (relative to the initial 2001 levels). Thus if there is a particular market that has a lot of 2001 Toyota Corollas the stock of available cars might fall differently than in a market with few 2001 Toyota Corollas. If we do this for every make, model, and year combination we can construct changes in the potential stock of cars available for trade in. Then, as dealers might be able to move supply around, we will use the potential stock in surrounding markets to instrument/proxy for the stock in the market of interest. This will not be directly related to demand in the given market, but might capture changes in the supply available in that market. 

\subsection{Strategy 2}
Rather than try and capture the probability of the car no longer being in the market, we can measure the rate at which a given make, model, and year is sold at the annual level. For example, we might see that 2001 Toyota Corollas are sold at a higher rate 4 years out rather than 5 or 6 years out. Thus a market that has a lot of 2001 Toyota Corollas might expect a larger increase in supply in 2005 than in 2006 or 2007 or than a market with fewer 2001 Toyota Corollas. If we do this for every make, model, and year combination we can construct changes in the potential stock of resale used cars. As before, we can use this changes in surrounding markets to instrument/proxy for the stock in the market of interest. This will not be directly related to demand in the given market of interest, but might capture changes in the supply available in that market. 

\subsection{Strategy 3} 
This would be similar to the strategy above but actually exploit the timing variation. For each make, model, and year combination we can estimate the rate at which the combination gets sold or newly registered in February-March. We can then think about cars likely to be traded-in and sold at tax refund time and construct the analogous market level measures.

\begin{equation}
S_{g,t} = \sum_{m=1}^M \frac{n_{m,yr,t}}{N_{m,yr}}*(R_{g,m,yr})
\end{equation}
\begin{itemize}
\item $m$: unique make and model combination (e.g., Toyota Corolla)
\item $yr$: Release year of vehicle (differentiates 2001 Toyota Corolla from 2002 Toyota Corolla) 
\item $n_{m,yr,t}$: Number of make/model/year combination registered in February-March of year $t$ in the state. This will tell us something about the transition path of a make/model/year over time. 
\item $N_{m,yr}$: Number of make/model/year combinations registered in the state in year of release ($yr$). This will give us a baseline so we can construct a share or survival ratio.
\item $R_{g,m,yr}$: Number of make/model/year combination registered in geography $g$ in year of release ($yr$). We could potentially change this to be the number registered in t-1 for example. I haven't totally thought through if this creates problems with bias.
\end{itemize}
Notice this actually is the opposite of a traditional shift share instrument. Typically we take an aggregate shift and multiply it by the shares of the geography g. Here we are taking an aggregate share and multiplying it by the number at the geography. I think it ends up having the same properties of a typical shift share though. We need to be concerned that the local levels of registered cars is correlated with trends.

We could also construct a supply measure by taking this supply measure in all of the surrounding clusters as follows

\begin{equation}
S_{g,t}=\sum_{i=1}^{G_{-g}}\sum_{m=1}^M \frac{n_{m,yr,t}}{N_{m,yr}}*(R_{g,m,yr})
\end{equation}
where $G_{-g}$ is the set of neighboring geographic clusters, what ever we define that to be.

\subsection{Testing the Strength of the proxies}
Although we cannot run the actual first stage (we don't observe supply) we can see if these measures affect market outcomes. We could look to see at times (away from tax season) if the stock of make, model, and year combination in neighboring markets affect the price of that make, model and year. If it reduces the price then we are likely to be observing shifts in the supply curve. We could also look at quantities for a given make, model, and year combination to see if quantities go up. 

I am not sure how to test things during the tax season given as there is no pre-period. If we don't interact with the EITC that might be ok because we are just looking at all markets, not necessarily strong EITC markets.



\end{document}
