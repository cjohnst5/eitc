/*
Author: Carla
Date: March 23, 2020
Description: File that spits out summary statistics
for car-level and zcta level data
*/



*=============================  Josh needs to change  ==========================
*Paths
global dataCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataCLEAN\"
*global dataCLEAN "F:\TxDMV Registrations data\dataCLEAN"
global codeCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\codeCLEAN\"
*global codeCLEAN "F:\TxDMV Registrations data\codeCLEAN"
global outputCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\outputCLEAN\"
*global outputCLEAN "F:\TxDMV Registrations data\outputCLEAN"


*Years
local by 2006
local ey 2007



*========================  Program  ============================================
*Opening log
local c_date = c(current_date)
local c_time = c(current_time)
local c_time_date = "`c_date'"+"_"+"`c_time'"
local time_string = subinstr("`c_time_date'", ":", "_", .)
local time_string = subinstr("`time_string'", " ", "_", .)
*starting a log file
capture log close
log using "$codeCLEAN/logFiles/sumstats`time_string'.log"


********************************************************************************
*Load price regressions table
insheet using "$dataCLEAN\priceregtables_`by'_`ey'.csv", clear

*Data cleaning/restrictions
qui destring lnprice, force replace
qui destring z_share_pcinc_eitc, force replace
qui destring z_share_hhinc_eitc, force replace
qui replace model = . if model==-1
qui replace make = . if make==-1


************************************	
*******Josh, Change stuff***********
************************************
*insert any more restrictions you made before running price regressions


*Number of observations
count

*Summary stats for price, lnprice, and miles
collapse (mean) avgprice=price avglnprice=lnprice avgmiles=miles ///
	(median) medprice=price medlnprice=lnprice medmiles=miles ///
	(sd) sdprice=price sdlnprice=lnprice sdmiles=miles

list
export delimited "$outputCLEAN\output\sumstats_priceregtables_`by'_`ey'.csv", replace



********************************************************************************
*Load quantity regressions table
insheet using "$dataCLEAN\qtyregtables_zcta_mnth_`by'_`ey'.csv", clear

*Data cleaning/restrictions
quietly destring share_pcinc_eitc, force replace
quietly destring share_hhinc_eitc, force replace


*Number of obs
count

*Summary stats for sales and ln sales
collapse (mean) avgsales=usedcarsales avglnsales=lnusedcarsales ///
	(median) medsales=usedcarsales medlnsales=lnusedcarsales ///
	(sd) sddsales=usedcarsales sdlnsales=lnusedcarsales	
list
export delimited "$outputCLEAN\output\sumstats_qtyregtables_`by'_`ey'.csv", replace



