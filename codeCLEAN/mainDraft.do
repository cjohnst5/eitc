set trace on
set tracedepth 1
set more off
timer clear
clear all
set maxvar 32000
set matsize 11000
set seed 123

sysuse auto
/*===========================================================================================*/
/*                                     Main Program                                          */
/*===========================================================================================*/
capture program drop main
program define main
		
	// ============== Load paths to folders ====================================
		paths
		
		
	// ============== change these variables ===================================
		local by 2004
		local ey 2010

    // =============== 0 Comment in/out subprograms you wish to run ============
	
	
	// ========= 1 Quantity regressions: zcta and month level ===============
	//Debugged with Josh
		openLog, name("quantity_zctam")
		* ===== Load data ===== *
		insheet using "$dataCLEAN\qtyregtables_zcta_mnth_`by'_`ey'.csv", clear
		quietly destring share_pcinc_eitc, force replace
		quietly destring share_hhinc_eitc, force replace
		
		* ===== Regressions ===== *
		quantityMonthTable, geography("zcta") eitcMeasure("##c.shareeitc")	
		quantityMonthTable, geography("zcta") eitcMeasure("##c.eitcdollars_percap")		
		quantityMonthTable, geography("zcta") eitcMeasure("##c.share_pcinc_eitc")
		quantityMonthTable, geography("zcta") eitcMeasure("##c.shareeitc##c.eitcdollars_percap")		
		
	// ========= Quantity regressions: county and month level ==================
	//Debugged with Josh	
		openLog, name("quantity_ctym")
		* ===== Load and clean data ===== *		
		insheet using "$dataCLEAN\qtyregtables_cty_mnth_`by'_`ey'.csv", clear
		
		* ===== Regressions ===== * 
		quantityMonthTable, geography("ctyfips") eitcMeasure("##c.shareeitc")	
		quantityMonthTable, geography("ctyfips") eitcMeasure("##c.eitcdollars_percap")
		quantityMonthTable, geography("ctyfips") eitcMeasure("##c.share_pcinc_eitc")
		quantityMonthTable, geography("ctyfips") eitcMeasure("##c.shareeitc##c.eitcdollars_percap")

	// ========= Price regressions =======================
	//Couldn't debug because limited zcta/year variation. Fingers crossed it works! 

		openLog, name("price_m")
		* ===== Load and clean data ===== * 
		insheet using "$dataCLEAN\priceregtables_`by'_`ey'.csv", clear
		qui destring lnprice, force replace
		qui destring z_share_pcinc_eitc, force replace
		qui destring z_share_hhinc_eitc, force replace
		qui replace model = . if model==-1
		qui replace make = . if make==-1
		
		* ===== Regressions: month, zcta FE =====* 
		priceMonthTable, geography("zcta") eitcMeasure("##c.z_shareeitc")	
		priceMonthTable, geography("zcta") eitcMeasure("##c.z_eitcdollars_percap")
		priceMonthTable, geography("zcta") eitcMeasure("##c.z_share_pcinc_eitc")
		priceMonthTable, geography("zcta") eitcMeasure("##c.z_shareeitc##c.z_eitcdollars_percap")


		* ===== Regressions: month, county FE =====* 
		priceMonthTable, geography("ctyfips") eitcMeasure("##c.z_shareeitc")	
		priceMonthTable, geography("ctyfips") eitcMeasure("##c.z_eitcdollars_percap")
		priceMonthTable, geography("ctyfips") eitcMeasure("##c.z_share_pcinc_eitc")
		priceMonthTable, geography("ctyfips") eitcMeasure("##c.z_shareeitc##c.c_eitcdollars_percap")
	
	
		* ====== Regressions: zipcode biweekely ===== * 
		*TODO: Modify program to take a frequency argument
		
		* ===== Regressions: county biweekly ===== * 
	
	// ======== Plots =====================================
/*
	cleanParmEst, parmEstFileName("qmtble_shareeitc_zcta_mxy.dta") ///
		parmEstFilePath("$outputCLEAN\output\")
		
	plotRegCoeffs, ///
		cpeFile("$outputCLEAN\output\cpe_qmtble_shareeitc_zcta_mxy.dta") ///
		outcomeVariable("quantitymonth") timeVariable("month") ///
		outputFile("$outputCLEAN\figures\prc_qmtble_shareeitc_zcta_mxy") ///
		eitcMeasure("shareeitc")
*/
	*Robustness Check quantity tables
	

	
	
	
end
// program main


/*===========================================================================================*/
/*                                    Sub Programs                                           */
/*===========================================================================================*/
 
/*---------------------------------------------------------*/
/* Define Path Macros 					                   */
/*---------------------------------------------------------*/
capture program drop paths
program define paths

	*CHANGE THESE PATHS BELOW TO YOUR COMPUTER DIRECTORIES
	
	*global dataCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataCLEAN\"
	global dataCLEAN "F:\TxDMV Registrations data\dataCLEAN"
	*global codeCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\codeCLEAN\"
	global codeCLEAN "F:\TxDMV Registrations data\codeCLEAN"
	*global outputCLEAN "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_clonedRepo\eitc\outputCLEAN\"
	global outputCLEAN "F:\TxDMV Registrations data\outputCLEAN"
	
	
end	

/*---------------------------------------------------------*/
/* Create a log file					                   */
/*---------------------------------------------------------*/

capture program drop openLog
program define openLog
syntax, [name(string)]

****Creating a string with current date and time
	local c_date = c(current_date)
	local c_time = c(current_time)
	local c_time_date = "`c_date'"+"_"+"`c_time'"
	local time_string = subinstr("`c_time_date'", ":", "_", .)
	local time_string = subinstr("`time_string'", " ", "_", .)
	*starting a log file
	capture log close
	log using "$codeCLEAN/logFiles/`name'`time_string'.log"

end

/*---------------------------------------------------------*/
**** Quantity Month ****
/*---------------------------------------------------------*/
capture program drop quantityMonthTable
program define quantityMonthTable
syntax, [geography(string) eitcMeasure(string)]	
	
	*Month*year interaction FE
	reg lnusedcarsales b7.mnth`eitcMeasure' ///
		i.`geography' mnth##year
	/*reghdfe lnusedcarsales b7.mnth`eitcMeasure', ///
		absorb(`geography' mnth##year)*/
		
	local msr = regexr("`eitcMeasure'", "##c.", "")
	local msr = regexr("`msr'", "##c.", "")
	parmest, saving("$outputCLEAN\output\qmtble_`msr'_`geography'", replace)	

end

/*---------------------------------------------------------*/
**** Price month ****
/*---------------------------------------------------------*/
capture program drop priceMonthTable
program define priceMonthTable
syntax, [geography(string) eitcMeasure(string)]
	
	*Month*year interaction FE
	/*reg lnprice b7.month`eitcMeasure' miles miles2 i.`geography' ///
		month##year i.make i.model i.modelyr, vce(cluster zcta)*/
		
	reg lnprice b7.month`eitcMeasure' miles miles2 i.`geography' ///
		month##year i.model#i.modelyr, vce(cluster zcta)	
		
	/*reghdfe lnprice b7.month`eitcMeasure' miles miles2, ///
		absorb(i.`geography' i.month#i.year i.year  /// 
		i.model#i.modelyr) vce(cluster zcta)*/
		
	local msr = regexr("`eitcMeasure'", "##c.", "")
	local msr = regexr("`msr'", "##c.", "")
	local msr = regexr("`msr'", "##c.", "")
	parmest, saving("$outputCLEAN\output\pmtble_`msr'_`geography'", replace)
	
end

/*---------------------------------------------------------*/
**** Clean parmest file ****
/*---------------------------------------------------------*/
capture program drop cleanParmEst
program define cleanParmEst
syntax, [parmEstFileName(string) parmEstFilePath(string)]
	/* This only works for non-interacted eitcmeasures. 
	*/
	
	local filetoload "`parmEstFilePath'\`parmEstFileName'"
	use `filetoload', clear
	gen plotvars = regexm(parm, "nth#c.")
	keep if plotvars == 1
	gen month = substr(parm, 1, 1)
	destring month, replace
	
	save "`parmEstFilePath'\cpe_`parmEstFileName'", replace
	

end
/*---------------------------------------------------------*/
**** Plot regression coefficients ****
/*---------------------------------------------------------*/
capture program drop plotRegCoeffs
program define plotRegCoeffs
syntax, [cpeFile(string) outcomeVariable(str) timeVariable(string) ///
	outputFile(string) eitcMeasure(string)]
	
	/*Right now outcomeVariable and timeVariable aren't working
	because variable names can't be passed as strings. I hard coded
	parm and month for a workaround.
	*/
	local yaxis "`outcomeVariable' `eitcMeasure'"
	
	use `cpeFile', clear
	twoway (connected estimate month, yvarlabel("`yaxis'")), yline(0) 
	*TODO: Add a note about the base time period
	
stop	
	local graphName plotRegCoeffs_`eitcMeasure'_`timeVariable'
	graph save "$figures\`graphName'.gph", replace
	graph export "$figures\`graphName'.pdf", replace

end

/*---------------------------------------------------------*/
**** Average monthly car prices ****
/*---------------------------------------------------------*/
capture program drop avgMonthlyPrices
program define avgMonthlyPrices
/*This program gets the average monthly prices for each zcta, controlling
for car characteristics and year. 
Eventually these will be used in the bin scatter plot of avgFebprice/avg monthly price
regressed on avg eitc return/per household income

I'm not sure if I'll need this, so everything is in very pseudo code form
How to not include a constant: https://www.stata.com/features/overview/factor-variables/ */
	
	insheet using "$dataCLEAN/priceregtables_2004_2010.csv, clear
	*import delimited "$dataCLEAN/priceregtables_2004_2010.csv", clear
	reg vehsalesprice ibn.month#ibn.zcta i.year i.model#i.make#i.modelyr, noconstant

end

/*---------------------------------------------------------*/
 /* Run Main Program                                       */
 /*--------------------------------------------------------*/


main
//main program
exit
