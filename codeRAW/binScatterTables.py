# Author: Carla
# Date created: 2/10/2020
# Description: Creates the tables needed for bin scatter plots

import pandas as pd


# ==============================================================================
# GLOBALS
# ==============================================================================

# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"
    dataCLEAN = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataCLEAN/"

    begin_year = 2004
    end_year = 2004
    # ==================Other vars (don't need to change)=======================
    qtyreg_file = dataCLEAN + \
                  'qtyregtables_zcta_mnth_{}_{}.csv'.\
                      format(begin_year, end_year)
    yrly_carsales_file = dataTEMP + 'zcta_data/' + \
                         'dmv_usdcarsales_zctayr_{}_{}.csv'.\
                             format(begin_year, end_year)

    output_file = dataCLEAN + \
                  'binScatterTable_zctamnth_{}_{}.csv'.\
                    format(begin_year, end_year)

    # ==========================================================================
    qtyreg_cols = chooseQtyRegVars()
    qtyreg_df = pd.read_csv(qtyreg_file, usecols=qtyreg_cols)
    yrly_carsales_df = pd.read_csv(yrly_carsales_file)

    feb_mar_sales = reshapeLongToWide(qtyreg_df)
    feb_mar_sales = mergeData(feb_mar_sales, yrly_carsales_df)
    createNewVars(feb_mar_sales)
    outputDf(feb_mar_sales, output_file)





# ==============================================================================
# Sub programs
# ==============================================================================
def chooseQtyRegVars():
    col_names = ['zcta', 'mnth', 'year', 'usedcarsales',
                 'shareEITC', 'eitcdollars_percap',
                 'eitcdollars_perhh', 'eitctakeup_rate', 'share_pcinc_eitc',
                 'share_hhinc_eitc']
    return col_names


def reshapeLongToWide(df):
    feb_bool = (df['mnth'] == 2)
    mar_bool = (df['mnth'] == 3)
    df_feb = df.loc[feb_bool, ['zcta', 'year', 'usedcarsales']]
    df = df.loc[mar_bool]
    df.drop(columns='mnth', inplace=True)

    df = df.merge(df_feb, left_on=['zcta', 'year'],
                              right_on=['zcta', 'year'])
    df.rename(columns={'usedcarsales_x': 'mar_sales',
                       'usedcarsales_y': 'feb_sales'},
              inplace=True)

    return df


def mergeData(qtyreg_wdf, yrly_sales_df):
    merged_df = qtyreg_wdf.merge(yrly_sales_df,
                                how='inner',
                                left_on=['zcta', 'year'],
                                right_on=['zcta', 'vehsolddateyr'])
    merged_df.rename(columns={'usedcarsales': 'yr_usedcarsales'},
                     inplace=True)
    merged_df.drop(columns='vehsolddateyr', inplace=True)
    return merged_df


def createNewVars(df):
    df['avg_mnth_sales'] = df['yr_usedcarsales'] / 12
    df['avg_2mnth_sales'] = df['avg_mnth_sales'] * 2
    df['feb_share'] = df['feb_sales'] / df['avg_mnth_sales']
    df['mar_share'] = df['mar_sales']/ df['avg_mnth_sales']
    df['feb_mar_share'] = (df['feb_sales'] + df['mar_sales'])/df['avg_2mnth_sales']


def outputDf(df, output_filename):
    df.to_csv(output_filename, index=False, sep=',')

# ==============================================================================
# Execute
# ==============================================================================
main_program()
