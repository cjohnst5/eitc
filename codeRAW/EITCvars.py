# Author: Carla
# Date created: 2/1/2020
# Description: This script creates the zcta and county yearly eitc variables
# we need for our analysis
# This is working and our scripts are created!

import pandas as pd
import numpy as np
# ==============================================================================
# Main program function
# ==============================================================================
def main_program():
    # ==================Variables you need to set ==============================

    # Paths
    dataRAW = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataRAW/"
    dataTEMP = "C:/Users/Daniel and Carla/Dropbox/projects/eitc/EITC_clonedRepo/eitc/dataTEMP/"

    # Input files/file paths
    eitc_path = dataRAW + 'EITC_takeup_brookings/'
    zip_zcta_cty_xwalk = dataRAW + 'crosswalks/zip_zcta_cty_xwalk_2015.csv'
    zcta_pop2010 = dataTEMP + 'zcta_data/zcta2010demo.csv'
    zcta_pop2000 = dataTEMP + 'zcta_data/zcta2000demo.csv'
    price_inflator = dataRAW + 'priceinflator/pce_inflationyr_all.dta'

    # output file/file paths
    zctaEITC_file = dataTEMP + 'zcta_data/eitcVars_zcta_allyrs.csv'
    ctyEITC_file = dataTEMP + 'cty_data/eitcVars_cty_allyrs.csv'
    # ==========================================================================
    eitc_all_yrs = appendEITCyrs(input_file_path=eitc_path,
                  crosswalk_file=zip_zcta_cty_xwalk,
                  eitc_zip_var='Zipcode')
    zcta_eitc = collapseToZCTA(eitc_all_yrs)
    zcta_eitc = mergeZCTA2010demo(zcta_eitc, zcta_pop2010)
    zcta_eitc = mergeZCTA2000demo(zcta_eitc, zcta_pop2000)

    zcta_eitc = priceInflationAdj(price_inflator, zcta_eitc)


    cty_eitc = collapseToCounty(zcta_eitc)
    cty_eitc = priceInflationAdj(price_inflator, cty_eitc)

    zcta_eitc = createNewVars(zcta_eitc, 'zcta')
    cty_eitc = createNewVars(cty_eitc, 'cty')

    exportEITCdf(zcta_eitc, zctaEITC_file)
    exportEITCdf(cty_eitc, ctyEITC_file)




# TODO: make sure exports are working
# ==============================================================================
# Sub programs
# ==============================================================================
def appendEITCyrs(input_file_path, crosswalk_file, eitc_zip_var):
    eitcVars04_07, eitcVars08_10 = chooseEITCVars()

    eitc_df = loadFile(input_file_path, '/zipcode04.csv', eitcVars04_07)
    eitc_df['year'] = 2004
    eitc_df = mergeCrosswalks(eitc_df, crosswalk_file, eitc_zip_var)

    for year in ['05', '06', '07', '08', '09', '10']:
        if int(year) <=7:
            eitc_vars = eitcVars04_07
        else:
            eitc_vars = eitcVars08_10
        print(year)

        eitc_yr = loadFile(input_file_path, '/zipcode{}.csv'.format(year), eitc_vars)
        eitc_yr['year'] = 2000 + int(year)
        eitc_yr = mergeCrosswalks(eitc_yr, crosswalk_file, eitc_zip_var)

        eitc_df = eitc_df.append(eitc_yr, ignore_index=True)

    removeCommas(eitc_df)
    return eitc_df

def chooseEITCVars():
    zip = ['Zipcode']

    tax_return_vars = ['treturn', 'tnew', 'teic', 'teicam', 'tctc', 'tctcam',
                       'tref', 'trefam', 'tral', 'tself', 'tpaid', 'tvol']

    tax_return_agi = ['tagi0_', 'tagi5_', 'tagi10_', 'tagi15_', 'tagi20_',
                      'tagi25_', 'tagi30_', 'tagi35_', 'tagi40_', 'tagi50_',
                      'tagi60_', 'tagi75_', 'tagi1k_']

    eitc_return_vars = ['ereturn', 'enew', 'eeic', 'eeicam', 'ectc', 'ectcam',
                        'eref', 'erefam', 'eral', 'eself', 'epaid', 'evol']

    eitc_return_agi_04_07 = ['eagi0_', 'eagi5_', 'eagi10_', 'eagi15_',
                             'eagi20_',
                             'eagi25_', 'eagi30_', 'eagi35_']
    eitc_return_agi_08_10 = eitc_return_agi_04_07 + ['eagi40_']

    col_names_04_07 = zip + tax_return_vars + tax_return_agi + eitc_return_vars + \
                      eitc_return_agi_04_07
    col_names_08_10 = zip + tax_return_vars + tax_return_agi + eitc_return_vars + \
                      eitc_return_agi_08_10

    return col_names_04_07, col_names_08_10

def loadFile(file_path, file_name, file_vars):
    file = file_path + file_name
    df = pd.read_csv(file, usecols=file_vars, low_memory=False)
    return df


def mergeCrosswalks(df, crosswalk_file, df_key):
    zip_zcta_cty_xwalk = pd.read_csv(crosswalk_file)

    merged = df.merge(zip_zcta_cty_xwalk, how='inner',
                      left_on=df_key,
                      right_on='zipcode')
    merged.drop(['zipcode'], axis=1, inplace=True)

    return merged

def removeCommas(df):
    str_cols = [c for c in df.columns if df.dtypes[c] == 'object']
    for c in str_cols:
        df[c] = df[c].str.replace(',', '').astype(float)

def collapseToZCTA(df):
    tax_vars = chooseEITCVars()[1][1:]
    sum_func = ['sum' for t in tax_vars]
    agg_dict = dict(zip(tax_vars, sum_func))
    agg_dict.update({'ctyfips': 'first'})

    zcta_eitc_vars = df.groupby(['zcta', 'year'], as_index=False).agg(agg_dict)
    return zcta_eitc_vars


def mergeZCTA2010demo(zcta_df, file_name):
    zcta_pop_vars = pd.read_csv(file_name, usecols=['zcta', 'zcta_pop2010', 'zcta_totalhh2010'])
    # eitc files don't contain 3 zcta codes. Otherwise everything matches
    zcta_df = zcta_df.merge(zcta_pop_vars, how='inner',
                            left_on='zcta',
                            right_on='zcta')
    return zcta_df


def mergeZCTA2000demo(zcta_df, file_name):
    zcta_pop_vars = pd.read_csv(file_name, usecols=['zcta', 'zcta_pop2000',
                                                    'zcta_totalhh2000',
                                                    'zcta_medhhinc2000adj',
                                                    'zcta_percapinc2000adj'])
    zcta_df = zcta_df.merge(zcta_pop_vars, how='inner',
                            left_on='zcta',
                            right_on='zcta')
    return zcta_df

def priceInflationAdj(inflator_filename, df):

    inflator_df = pd.read_stata(inflator_filename)
    yr_range = (inflator_df['year'] >=2004) & (inflator_df['year']<=2010)
    inflator_df = inflator_df.loc[yr_range, :]

    df = df.merge(inflator_df, how='inner',
                            left_on = 'year',
                            right_on = 'year')

    df['teicam_adj'] = df['teicam'] * df['pce_inflator']
    df.drop(columns='pce_inflator', inplace=True)

    return df

def collapseToCounty(df):
    # Variable lists
    tax_vars = chooseEITCVars()[1][1:]
    pop_vars = ['zcta_pop2010', 'zcta_totalhh2010',
                 'zcta_pop2000', 'zcta_totalhh2000']
    inc_vars = ['zcta_percapinc2000adj', 'zcta_medhhinc2000adj']

    # aggregation dictionaries
    sum_vec = ['sum' for t in tax_vars + pop_vars]
    sum_dict = dict(zip(tax_vars+pop_vars, sum_vec))

    wm_pc00 = lambda x: np.average(x, weights=df.loc[x.index, 'zcta_pop2000'])
    wm_hh00 = lambda x: np.average(x, weights=df.loc[x.index, 'zcta_totalhh2000'])

    wght_means = [{'weighted_mean': wm_pc00}, {'weighted_mean': wm_hh00}]
    wght_means_dict = dict(zip(inc_vars, wght_means))

    agg_dict = sum_dict
    agg_dict.update(wght_means_dict)


    # Column names dictionary
    tax_names = [t for t in tax_vars]
    pop_names = ['cty_pop2010', 'cty_totalhh2010',
                  'cty_pop2000', 'cty_totalhh2000']
    inc_names = ['cty_percapinc2000adj', 'cty_medhhinc2000adj']
    names_dict = dict(zip(tax_vars+pop_vars+inc_vars,
                          tax_names+pop_names+inc_names))

    # Collapsing to county level
    cty_df = df.groupby(['ctyfips', 'year'], as_index=False).\
        agg(agg_dict, name=names_dict)
    cty_df.columns = cty_df.columns.droplevel(level=1)
    cty_df.rename(columns=names_dict, inplace=True)

    return cty_df

def createNewVars(df, geo_var):
    df['shareEITC'] = (df['teic']/df['treturn'])*100
    df['avgeitc_rtrn'] = df['teicam_adj']/df['ereturn'].replace(0, 1)
    df['eitcdollars_percap'] = df['teicam_adj']/\
                               df['{}_pop2010'.format(geo_var)].replace(0, 1)
    df['eitcdollars_perhh'] = df['teicam_adj']/\
                              df['{}_totalhh2010'.format(geo_var)].replace(0, 1)
    df['eitctakeup_rate'] = (df['teic']/
                             df['{}_totalhh2010'.format(geo_var)].replace(0, 1))\
                            *100
    df['share_pcinc_eitc'] = (df['avgeitc_rtrn']/
                              df['{}_percapinc2000adj'.format(geo_var)])*100
    df['share_pcinc_eitc'].replace(np.inf, np.nan, inplace=True)

    df['share_hhinc_eitc'] = (df['avgeitc_rtrn']/
                              df['{}_medhhinc2000adj'.format(geo_var)])*100
    df['share_hhinc_eitc'].replace(np.inf, np.nan, inplace=True)

    # Interaction terms
    df['shareXdollars_perhh'] = df['shareEITC']*df['eitcdollars_perhh']

    # Takeup rates more than 100% (mostly PO zipcodes)
    df = df.loc[df['eitctakeup_rate'] <=100, :]

    return df

def dropMiss(df):
    df = df.loc[df['shareEITC'] ]
def exportEITCdf(df, output_file_name):
    df.to_csv(output_file_name, sep=',', index=False)




# ==============================================================================
# Execute
# ==============================================================================
main_program()