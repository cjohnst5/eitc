*Author: Carla
*Date Jan 30, 2020
*This creates a crosswalk between zcta, zip and county codes
*Also creates a zcta level data set of household and population

cd "C:\Users\Daniel and Carla\Dropbox\projects\eitc\EITC_carprice\dataRAW\EITC_takeup_brookings"
use zipcta_cty_xwalk, clear
keep zipcta ctyfips1 stfips1

merge 1:m zipcta using zip_zcta_xwalk2015, keepusing(zipcode) nogen

* Zipcode level populations
keep if stfips == 48
rename zipcta zcta 
rename ctyfips1 ctyfips
drop stfips1

*Zip-zcta-cty crosswalk
export delimited "C:\Users\carli\Dropbox\projects\eitc\EITC_clonedRepo\eitc\dataRAW\crosswalks\zip_zcta_cty_xwalk_2015.csv", replace


